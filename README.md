# midigen

Generate midi files from JSON describing a song's structure and control changes

Very early beta.

See `example.json` for an example of the JSON format.
